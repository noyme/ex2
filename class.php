<?php
/* ex1 */

    class Htmlpage {
        protected $title = "Titel";
        protected $body = "Body";
        function __construct($title = "", $body =""){
            if($title !=""){
                $this->title = $title;
            }
            if($body !=""){
                $this->body = $body;
            }
        }
        public function view (){
            echo "<html>
                <head>
                    <title>
                    $this->title
                 </title>
                </head>
                <body>
                    $this->body
                </body>
            </html>";
        }
        
    }
/* ex2 */
    class coloredText extends Htmlpage {
        protected $color = '';
        public function __set($property,$value){
            if ($property == 'color'){
                $colors = array('red','yellow','green');
                if (in_array($value,$colors)){
                    $this->color = $value;
                }
                else {
                    die( 'Error!! choose diffrent color');
                }
            }
        }

        public function view(){
                echo
                    "<html>
                    <head>
                    <title>
                        $this->title
                    </title>
                    </head>
                    <body>
                    <p style = 'color:$this->color'>$this->body</p>
                    </body>
                    </html>";
            }
        
    }
/* ex3 */
    class fontSize extends coloredText {
        protected $font = '';
        public function __set($property,$value){
            parent::__set($property,$value);
            if ($property == 'font'){
                $sizeRange = range(10,24);
                if (in_array($value,$sizeRange)){
                    $this->font = $value;
                }
                else {
                    die( 'Erorr!! choose diffrent font size');
                }
            }
        }
        public function view(){
                echo
                    "<html>
                    <head>
                    <title>
                        $this->title
                    </title>
                    </head>
                    <body>
                    <p style = 'color:$this->color;font-size:$this->font'>$this->body </p>
                    </body>
                    </html>";
            
        }

    }
    

?>
